<?php 
    //Template Name: Artes 
?>
<?php get_header(); ?>
<section>
    <div id="page-artes" class="container">
        <h1>Artes</h1>
        <div id="card_images">
            <?php if (have_posts()) : while ( have_posts()): the_post(); ?>
            <?php the_content()?>
            <?php endwhile; else: ?>
                <p><?php _e('Não há artes postadas');?></p>
            <?php endif; ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>