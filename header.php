<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Overlock&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Overlock+SC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|UnifrakturCook:700&display=swap" rel="stylesheet"> 
    
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/css/reset.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/style.css">
    <title>Bell's</title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="container">
            <a href="/"><img src="<?php echo get_stylesheet_directory_uri()?>/img/Bell’s.png"></a> 
            <nav>
                <a href="">Livros</a>
                <a href="/Artes/">Artes</a>
                <a href="">Sobre</a>
                <a href="/Contatos/">Contatos</a>
                <a href="">Login</a>
                <a href=""><button class="button">Doar</button></a>
            </nav>
        </div>
    </header>

