<footer>
    <div class="container">
        <div>
            <nav id="footer_nav1">
                <a href="">Sobre</a>
                <a href="">Artes</a>
                <a href="">Livros</a><br>
                <a href="">Login</a>
                <a href="">Contatos</a>
                <a href=""></a>
            </nav>
            <nav id="footer_nav2">
                <img src="<?php echo get_stylesheet_directory_uri()?>/img/face.png" alt="">
                <img src="<?php echo get_stylesheet_directory_uri()?>/img/insta.png" alt="">
                <img src="<?php echo get_stylesheet_directory_uri()?>/img/twitter.png" alt="">
                <img src="<?php echo get_stylesheet_directory_uri()?>/img/pinterest.png" alt="">
            </nav>
        </div>
        <div id="footer_div2">
            <p> Desenvolvido por </p>
            <img src="<?php echo get_stylesheet_directory_uri()?>/img/Group.png" alt="">
        </div>
    </div>
    <div id="copyright">
        <p>© Copyright 2020 Bell’s Books</p>
    </div>

</footer>

    <?php wp_footer() ?>
</body>
</html>